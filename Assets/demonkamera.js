﻿var rotatetarget:Transform;
var xspeed:float = 100;
var yspeed:float = 100;
var targetObj:Transform;
var distance = 10.0;
var dy = 1.0;
 
private var x = 0.0;
private var y = 0.0;
 
var yMinLimit = -20;
var yMaxLimit = 80;
 
function Start () {
    var angles = rotatetarget.eulerAngles;
    x = angles.y;
    y = angles.x;
 
    // Make the rigid body not change rotation
    if (rigidbody)
        rigidbody.freezeRotation = true;
}
 
function Update() {
 
    //Drag
    if(Input.GetMouseButton(0)) {
 
        x += Input.GetAxis("Mouse X") * xspeed * 0.1;
        y -= Input.GetAxis("Mouse Y") * yspeed * 0.1;
 
        y = ClampAngle(y , yMinLimit, yMaxLimit);
 
        var rotation = Quaternion.Euler(y, x, 0);
        var position = rotation * Vector3(0.0, dy, -distance) + targetObj.position;
 
        rotatetarget.rotation = rotation;
        rotatetarget.position = position;
 
    }
 
    rotatetarget.position = Quaternion.Euler(y, x, 0) * Vector3(0.0, dy, -distance) + targetObj.position;
 
}
 
static function ClampAngle (angle : float, min : float, max : float) {
    if (angle < -360)
        angle += 360;
    if (angle > 360)
        angle -= 360;
    return Mathf.Clamp (angle, min, max);
}