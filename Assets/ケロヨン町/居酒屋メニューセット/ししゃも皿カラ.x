xof 0302txt 0064
template Header {
 <3D82AB43-62DA-11cf-AB39-0020AF71E433>
 WORD major;
 WORD minor;
 DWORD flags;
}

template Vector {
 <3D82AB5E-62DA-11cf-AB39-0020AF71E433>
 FLOAT x;
 FLOAT y;
 FLOAT z;
}

template Coords2d {
 <F6F23F44-7686-11cf-8F52-0040333594A3>
 FLOAT u;
 FLOAT v;
}

template Matrix4x4 {
 <F6F23F45-7686-11cf-8F52-0040333594A3>
 array FLOAT matrix[16];
}

template ColorRGBA {
 <35FF44E0-6C7C-11cf-8F52-0040333594A3>
 FLOAT red;
 FLOAT green;
 FLOAT blue;
 FLOAT alpha;
}

template ColorRGB {
 <D3E16E81-7835-11cf-8F52-0040333594A3>
 FLOAT red;
 FLOAT green;
 FLOAT blue;
}

template IndexedColor {
 <1630B820-7842-11cf-8F52-0040333594A3>
 DWORD index;
 ColorRGBA indexColor;
}

template Boolean {
 <4885AE61-78E8-11cf-8F52-0040333594A3>
 WORD truefalse;
}

template Boolean2d {
 <4885AE63-78E8-11cf-8F52-0040333594A3>
 Boolean u;
 Boolean v;
}

template MaterialWrap {
 <4885AE60-78E8-11cf-8F52-0040333594A3>
 Boolean u;
 Boolean v;
}

template TextureFilename {
 <A42790E1-7810-11cf-8F52-0040333594A3>
 STRING filename;
}

template Material {
 <3D82AB4D-62DA-11cf-AB39-0020AF71E433>
 ColorRGBA faceColor;
 FLOAT power;
 ColorRGB specularColor;
 ColorRGB emissiveColor;
 [...]
}

template MeshFace {
 <3D82AB5F-62DA-11cf-AB39-0020AF71E433>
 DWORD nFaceVertexIndices;
 array DWORD faceVertexIndices[nFaceVertexIndices];
}

template MeshFaceWraps {
 <4885AE62-78E8-11cf-8F52-0040333594A3>
 DWORD nFaceWrapValues;
 Boolean2d faceWrapValues;
}

template MeshTextureCoords {
 <F6F23F40-7686-11cf-8F52-0040333594A3>
 DWORD nTextureCoords;
 array Coords2d textureCoords[nTextureCoords];
}

template MeshMaterialList {
 <F6F23F42-7686-11cf-8F52-0040333594A3>
 DWORD nMaterials;
 DWORD nFaceIndexes;
 array DWORD faceIndexes[nFaceIndexes];
 [Material]
}

template MeshNormals {
 <F6F23F43-7686-11cf-8F52-0040333594A3>
 DWORD nNormals;
 array Vector normals[nNormals];
 DWORD nFaceNormals;
 array MeshFace faceNormals[nFaceNormals];
}

template MeshVertexColors {
 <1630B821-7842-11cf-8F52-0040333594A3>
 DWORD nVertexColors;
 array IndexedColor vertexColors[nVertexColors];
}

template Mesh {
 <3D82AB44-62DA-11cf-AB39-0020AF71E433>
 DWORD nVertices;
 array Vector vertices[nVertices];
 DWORD nFaces;
 array MeshFace faces[nFaces];
 [...]
}

Header{
1;
0;
1;
}

Mesh {
 28;
 0.17280;0.02425;0.07372;,
 -0.17280;0.02425;0.07372;,
 -0.17280;0.01528;0.07372;,
 0.17280;0.01528;0.07372;,
 0.17280;0.02425;-0.11623;,
 0.17280;0.01528;-0.11623;,
 -0.17280;0.02425;-0.11623;,
 -0.17280;0.01528;-0.11623;,
 -0.14712;-0.00260;-0.10211;,
 0.14712;-0.00260;-0.10211;,
 0.14712;-0.00260;0.05961;,
 -0.14712;-0.00260;0.05961;,
 -0.17280;0.01528;-0.11623;,
 0.17280;0.01528;-0.11623;,
 0.14712;-0.00260;-0.10211;,
 -0.14712;-0.00260;-0.10211;,
 0.17280;0.01528;-0.11623;,
 0.17280;0.01528;0.07372;,
 0.14712;-0.00260;0.05961;,
 0.14712;-0.00260;-0.10211;,
 0.17280;0.01528;0.07372;,
 -0.17280;0.01528;0.07372;,
 -0.14712;-0.00260;0.05961;,
 0.14712;-0.00260;0.05961;,
 -0.17280;0.01528;0.07372;,
 -0.17280;0.01528;-0.11623;,
 -0.14712;-0.00260;-0.10211;,
 -0.14712;-0.00260;0.05961;;
 
 10;
 4;0,1,2,3;,
 4;4,0,3,5;,
 4;6,4,5,7;,
 4;1,6,7,2;,
 4;1,0,4,6;,
 4;8,9,10,11;,
 4;12,13,14,15;,
 4;16,17,18,19;,
 4;20,21,22,23;,
 4;24,25,26,27;;
 
 MeshMaterialList {
  8;
  10;
  6,
  6,
  6,
  6,
  6,
  6,
  6,
  6,
  6,
  6;;
  Material {
   0.800000;0.800000;0.800000;1.000000;;
   5.000000;
   0.000000;0.000000;0.000000;;
   0.000000;0.000000;0.000000;;
   TextureFilename {
    "iztex01.png";
   }
  }
  Material {
   0.668000;0.633600;0.351200;1.000000;;
   5.000000;
   0.000000;0.000000;0.000000;;
   0.258850;0.245520;0.136090;;
   TextureFilename {
    "iztex01.png";
   }
  }
  Material {
   0.298400;0.125600;0.112800;1.000000;;
   18.000000;
   0.200000;0.200000;0.200000;;
   0.134280;0.056520;0.050760;;
   TextureFilename {
    "iztex01.png";
   }
  }
  Material {
   0.061410;0.011730;0.011730;1.000000;;
   30.000000;
   0.300000;0.300000;0.300000;;
   0.000000;0.000000;0.000000;;
   TextureFilename {
    "iztex01.png";
   }
  }
  Material {
   0.800000;0.166400;0.000000;1.000000;;
   5.000000;
   0.450000;0.450000;0.450000;;
   0.000000;0.000000;0.000000;;
   TextureFilename {
    "iztex01.png";
   }
  }
  Material {
   0.800000;0.800000;0.800000;1.000000;;
   5.000000;
   0.100000;0.100000;0.100000;;
   0.390000;0.390000;0.390000;;
   TextureFilename {
    "iztex01.png";
   }
  }
  Material {
   0.800000;0.800000;0.800000;1.000000;;
   26.000000;
   0.410000;0.410000;0.410000;;
   0.400000;0.400000;0.400000;;
   TextureFilename {
    "iztex01.png";
   }
  }
  Material {
   0.800000;0.800000;0.800000;0.300000;;
   54.000000;
   0.710000;0.710000;0.710000;;
   0.750000;0.750000;0.750000;;
   TextureFilename {
    "iztex01.png";
   }
  }
 }
 MeshNormals {
  12;
  -0.565232;-0.517995;0.642023;,
  0.565232;-0.517995;0.642023;,
  -0.565232;-0.518004;-0.642016;,
  0.565232;-0.518004;-0.642016;,
  -0.577350;0.577350;0.577350;,
  0.577350;0.577350;0.577350;,
  -0.577350;0.577350;-0.577350;,
  0.577350;0.577350;-0.577350;,
  -0.217602;-0.929148;-0.298888;,
  0.217602;-0.929148;-0.298888;,
  0.217604;-0.929144;0.298898;,
  -0.217604;-0.929144;0.298898;;
  10;
  4;5,4,0,1;,
  4;7,5,1,3;,
  4;6,7,3,2;,
  4;4,6,2,0;,
  4;4,5,7,6;,
  4;8,9,10,11;,
  4;2,3,9,8;,
  4;3,1,10,9;,
  4;1,0,11,10;,
  4;0,2,8,11;;
 }
 MeshTextureCoords {
  28;
  0.702170;0.598340;,
  0.426310;0.598340;,
  0.426310;0.598340;,
  0.702170;0.598340;,
  0.702170;0.796410;,
  0.702170;0.796410;,
  0.426310;0.796410;,
  0.426310;0.796410;,
  0.641750;0.798220;,
  0.493900;0.798220;,
  0.493900;0.600570;,
  0.641750;0.600570;,
  0.693100;0.597960;,
  0.434750;0.597960;,
  0.453950;0.613110;,
  0.673900;0.613110;,
  0.693100;0.597960;,
  0.434750;0.597960;,
  0.453950;0.613110;,
  0.673900;0.613110;,
  0.693100;0.597960;,
  0.434750;0.597960;,
  0.453950;0.613110;,
  0.673900;0.613110;,
  0.693100;0.597960;,
  0.434750;0.597960;,
  0.453950;0.613110;,
  0.673900;0.613110;;
 }
}
